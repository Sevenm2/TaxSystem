﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tax.Domain.Query
{
    public class CityInfoQuery
    {
        public string CityCode { get; set; }
    }
}
