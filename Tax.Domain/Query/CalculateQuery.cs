﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tax.Domain.Query
{
    public class CalculateQuery
    {
        public string LoginID { get; set; }

        public string CityCode { get; set; }

        public decimal Salary { get; set; }
    }
}
