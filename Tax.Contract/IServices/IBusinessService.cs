﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Tax.Contract.IServices
{
    using Tax.Domain.Query;
    using Tax.Domain.Model;

    public interface IBusinessService
    {
        Task<CityInfoListModel> GetCityInfo();

        Task<CalculateModel> CalculateTaxSalary(CalculateQuery query);
    }
}
