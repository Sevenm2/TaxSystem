USE master
GO

----------------------------------------------------------------------------------------------------

/*
--***删除DB***--

EXEC msdb.dbo.sp_delete_database_backuphistory @database_name = N'Taxation'
GO
USE master
GO
ALTER DATABASE Taxation SET SINGLE_USER WITH ROLLBACK IMMEDIATE
GO
USE master
GO
DROP DATABASE Taxation
GO

*/

----------------------------------------------------------------------------------------------------
declare @dir nvarchar(256)
set @dir = 'E:\Data\SQLServer\';

IF NOT EXISTS (SELECT 1 FROM sys.databases WHERE name = 'Taxation') 
BEGIN
--Default collate: Chinese_PRC_CI_AS
EXECUTE ('
CREATE DATABASE Taxation ON PRIMARY
	( NAME = N''Taxation'', FILENAME = N''' + @dir + 'Taxation.mdf'', SIZE = 20480KB, MAXSIZE = UNLIMITED, FILEGROWTH = 10%),
FILEGROUP Taxation_Index 
( NAME = N''Taxation_Index'', FILENAME = N''' + @dir + 'Taxation_index.ndf'', SIZE = 10240KB, MAXSIZE = UNLIMITED, FILEGROWTH = 10%)
LOG ON
	( NAME = N''Taxation_log'', FILENAME = N''' + @dir + 'Taxation_log.ldf'', SIZE = 10240KB, MAXSIZE = 1024GB, FILEGROWTH = 10%)
COLLATE Chinese_PRC_CI_AS
--SQL_Latin1_General_CP1_CI_AS
')
END
GO

ALTER DATABASE Taxation SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
	EXEC Taxation.dbo.sp_fulltext_database @action = 'enable'
GO

----------------------------------------------------------------------------------------------------

ALTER DATABASE Taxation SET ANSI_NULL_DEFAULT OFF
ALTER DATABASE Taxation SET ANSI_WARNINGS OFF
ALTER DATABASE Taxation SET ARITHABORT OFF
ALTER DATABASE Taxation SET AUTO_CLOSE OFF
ALTER DATABASE Taxation SET AUTO_CREATE_STATISTICS ON
ALTER DATABASE Taxation SET AUTO_SHRINK OFF
ALTER DATABASE Taxation SET AUTO_UPDATE_STATISTICS ON
ALTER DATABASE Taxation SET CURSOR_CLOSE_ON_COMMIT OFF
ALTER DATABASE Taxation SET CURSOR_DEFAULT  GLOBAL
ALTER DATABASE Taxation SET CONCAT_NULL_YIELDS_NULL OFF
ALTER DATABASE Taxation SET NUMERIC_ROUNDABORT OFF
ALTER DATABASE Taxation SET RECURSIVE_TRIGGERS OFF
ALTER DATABASE Taxation SET DISABLE_BROKER
ALTER DATABASE Taxation SET AUTO_UPDATE_STATISTICS_ASYNC OFF
ALTER DATABASE Taxation SET DATE_CORRELATION_OPTIMIZATION OFF
ALTER DATABASE Taxation SET TRUSTWORTHY OFF
ALTER DATABASE Taxation SET ALLOW_SNAPSHOT_ISOLATION OFF
ALTER DATABASE Taxation SET PARAMETERIZATION SIMPLE
ALTER DATABASE Taxation SET READ_COMMITTED_SNAPSHOT OFF
ALTER DATABASE Taxation SET HONOR_BROKER_PRIORITY OFF
ALTER DATABASE Taxation SET READ_WRITE
ALTER DATABASE Taxation SET RECOVERY FULL
ALTER DATABASE Taxation SET MULTI_USER
ALTER DATABASE Taxation SET PAGE_VERIFY CHECKSUM
ALTER DATABASE Taxation SET DB_CHAINING OFF
GO

EXEC sys.sp_db_vardecimal_storage_format N'Taxation', N'ON'
GO



USE Taxation
GO

----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
--City
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA='dbo' AND TABLE_NAME = 'City')
BEGIN
CREATE TABLE [dbo].[City](
	[CityID] [int] IDENTITY(1,1) NOT NULL,
	[CityCode] [varchar](16) NOT NULL,
	[CityName] [nvarchar](64) NOT NULL,
	[AnnuityRate] decimal(18,2) NOT NULL CONSTRAINT DF_City_AnnuityRate DEFAULT(0),
	[MedicareRate] decimal(18,2) NOT NULL CONSTRAINT DF_City_MedicareRate DEFAULT(0),
	[UnemployRate] decimal(18,2) NOT NULL CONSTRAINT DF_City_UnemployRate DEFAULT(0),
	[FundRate] decimal(18,2) NOT NULL CONSTRAINT DF_City_FundRate DEFAULT(0),
	[Deleted] [bit] NULL CONSTRAINT DF_City_Deleted DEFAULT(0),
	[CreatedTime] [datetime] NOT NULL CONSTRAINT DF_City_CreatedTime DEFAULT(GETDATE()),
	[LastUpdatedTime] [datetime] NOT NULL CONSTRAINT DF_City_LastUpdatedTime DEFAULT(GETDATE()),
 CONSTRAINT [PK_City] PRIMARY KEY CLUSTERED ( [CityID] ASC ) ON [PRIMARY],
 CONSTRAINT [UQ_City_CityCode] UNIQUE NONCLUSTERED 
(
	[CityCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
--User
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA='dbo' AND TABLE_NAME = 'User')
BEGIN
CREATE TABLE [dbo].[User](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[LoginID] [nvarchar](128) NOT NULL,
	[Password] [varchar](64) NOT NULL,
	[FirstName] [nvarchar](64) NOT NULL CONSTRAINT DF_User_FirstName DEFAULT(''),
	[LastName] [nvarchar](64) NOT NULL CONSTRAINT DF_User_LastName DEFAULT(''),
	[Email] [nvarchar](64) NOT NULL CONSTRAINT DF_User_Email DEFAULT(''),
	[DefaultQueryCityCode] [varchar](16) NULL,
	[LastLoginTime] [datetime] NULL,
	[Active] [bit] NULL CONSTRAINT DF_User_Active DEFAULT(1),
	[Deleted] [bit] NULL CONSTRAINT DF_User_Deleted DEFAULT(0),
	[CreatedTime] [datetime] NOT NULL CONSTRAINT DF_User_CreatedTime DEFAULT(GETDATE()),
	[LastUpdatedTime] [datetime] NOT NULL CONSTRAINT DF_User_LastUpdatedTime DEFAULT(GETDATE()),
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED ( [UserID] ASC ) ON [PRIMARY],
 CONSTRAINT [UQ_User_LoginID_Deleted] UNIQUE NONCLUSTERED 
(
	[LoginID] ASC, [Deleted] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

--------------------------------------------------
--TaxRate
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA='dbo' AND TABLE_NAME = 'TaxRate')
BEGIN
CREATE TABLE [dbo].[TaxRate](
	[TaxRateID] [int] IDENTITY(1,1) NOT NULL,
	[MoneyLevel] [int] NOT NULL,
	[Rate] [int] NOT NULL CONSTRAINT DF_TaxRate_Rate DEFAULT(0),
	[Offset] [int] NOT NULL CONSTRAINT DF_TaxRate_Offset DEFAULT(0),
	[Deleted] [bit] NULL CONSTRAINT DF_TaxRate_Deleted DEFAULT(0),
	[CreatedTime] [datetime] NOT NULL CONSTRAINT DF_TaxRate_CreatedTime DEFAULT(GETDATE()),
	[LastUpdatedTime] [datetime] NOT NULL CONSTRAINT DF_TaxRate_LastUpdatedTime DEFAULT(GETDATE()),
 CONSTRAINT [PK_TaxRate] PRIMARY KEY CLUSTERED ( [TaxRateID] ASC ) ON [PRIMARY]
) ON [PRIMARY]
END
GO
