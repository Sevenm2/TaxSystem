﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tax.Util
{
    public class SessionInfo
    {
        public static string LoginId
        {
            get
            {
                return WebHelper.GetSession();
            }
            set
            {
                if (value == null)
                {
                    WebHelper.RemoveSession();
                }
                else
                {
                    WebHelper.WriteSession(value: value);
                }
            }
        }
    }
}
