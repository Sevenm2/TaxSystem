﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tax.Domain.Enum
{
    public enum LoginResultEnum
    {
        SUCCESS = 1,
        NOTFOUND = 2,
        WRONGPASSWORD = 3
    }
}
