﻿using System;
using System.Collections.Generic;

namespace Tax.DataAccess.Models
{
    public partial class City
    {        
        public string CityCode { get; set; }
        public string CityName { get; set; }
        public decimal AnnuityRate { get; set; }
        public decimal MedicareRate { get; set; }
        public decimal UnemployRate { get; set; }
        public decimal FundRate { get; set; }
        public bool? Deleted { get; set; }
        //public DateTime CreatedTime { get; set; }
        //public DateTime LastUpdatedTime { get; set; }
    }
}
