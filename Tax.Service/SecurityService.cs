﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;

namespace Tax.Service
{
    using Tax.DataAccess.Models;
    using Tax.Contract.IServices;
    using Tax.Util;
    using Tax.Domain.Enum;
    using Tax.Domain.Model;
    using Tax.Domain.Query;
    public class SecurityService : ISecurityService
    {
        private TaxationContext _context;
        public SecurityService(TaxationContext context)
        {
            _context = context;
        }
        public Task<LoginModel> CheckLogin(LoginQuery query)
        {
            return Task.Run(async () =>
            {
                RedisUtils.SetAdd<string>(1, "aa", "bb");
                RedisUtils.StringSet<string>(1, "ac", "bb", TimeSpan.FromMinutes(30));
                LoginModel model = new LoginModel() { LoginID = query.LoginID};
                //var user = _context.User.Where(u => u.LoginId == query.LoginID).FirstOrDefault();
                User user = new User()
                {
                    Password = query.Password,
                    LoginId = query.LoginID
                };
                if (user != null)
                {
                    if (user.Password == query.Password)
                    {
                        model.LoginResult = (int)LoginResultEnum.SUCCESS;
                        SessionInfo.LoginId = user.LoginId;
                        try
                        {
                            List<string> ab = RedisUtils.GetList<string>(0, "ab");
                            Console.WriteLine(ab.Count);
                            var aa = await RedisHelper.GetListAsync<string>(0, "ab", null);
                        }
                        catch (Exception ex)
                        {
                        }
                        user.LastLoginTime = DateTime.UtcNow;
                        //_context.SaveChanges(true);
                    }
                    else
                    {
                        model.LoginResult = (int)LoginResultEnum.WRONGPASSWORD;
                    }
                }
                else
                {
                    model.LoginResult = (int)LoginResultEnum.NOTFOUND;
                }
                return model;
            });
        }

        public Task<LoginModel> Logout(LoginQuery query)
        {
            return Task.Run(() =>
            {
                LoginModel model = new LoginModel() { LoginID = query.LoginID };
                SessionInfo.LoginId = null;
                return model;
            });
        }
    }
}
