﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tax.Domain.Model
{
    public class CityInfoModel
    {
        public string CityName { get; set; }

        public string CityCode { get; set; }
    }

    public class CityInfoListModel
    {
        public string LoginID { get; set; }

        public string DefaultQueryCityCode { get; set; }

        public List<CityInfoModel> InfoList { get; set; }
    }
}
