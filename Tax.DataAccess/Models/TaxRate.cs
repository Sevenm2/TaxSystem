﻿using System;
using System.Collections.Generic;

namespace Tax.DataAccess.Models
{
    public partial class TaxRate
    {
        public int MoneyLevel { get; set; }
        public int Rate { get; set; }
        public int Offset { get; set; }
        public bool? Deleted { get; set; }
        //public DateTime CreatedTime { get; set; }
        //public DateTime LastUpdatedTime { get; set; }
    }
}
