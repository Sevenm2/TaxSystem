﻿using System;
using System.Collections.Generic;

namespace Tax.DataAccess.Models
{
    public partial class User
    {
        public string LoginId { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string DefaultQueryCityCode { get; set; }
        public DateTime? LastLoginTime { get; set; }
        public bool? Active { get; set; }
        public bool? Deleted { get; set; }
        //public DateTime CreatedTime { get; set; }
        //public DateTime LastUpdatedTime { get; set; }
    }
}
