﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tax.Domain.Query
{
    public class LoginQuery
    {
        public string LoginID { get; set; }

        public string Password { get; set; }
    }
}
