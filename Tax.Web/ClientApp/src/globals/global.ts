

import { environment } from '../environments/environment';
export const api = {
  checkLogin: environment.apiRoot + "api/security/checkLogin",
  logout: environment.apiRoot + "api/security/logout",
  getCityInfo: environment.apiRoot + "api/tax/getCityInfo",
  calculateTaxSalary: environment.apiRoot + "api/tax/calculateTaxSalary"
};