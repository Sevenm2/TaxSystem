﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Cors;

namespace Tax.API.Controllers
{
    using Tax.API.App_Start.Handler;
    using Tax.Contract.IServices;
    using Tax.Domain.Enum;
    using Tax.Domain.Model;
    using Tax.Domain.Query;

    [Route("api/[controller]")]
    [EnableCors("any")]
    [ApiController]
    [HandlerLogin(LoginMode.IGNORE)]
    public class SecurityController : ControllerBase
    {
        private ISecurityService _security;
        public SecurityController(ISecurityService security)
        {
            _security = security;
        }

        //POST: api/security/checkLogin
        [HttpPost, Route("checkLogin")]
        public async Task<IActionResult> CheckLogin(LoginQuery query)
        {
            var result = await _security.CheckLogin(query);
            return Ok(result);
        }

        //POST: api/security/logout
        [HttpPost, Route("logout")]
        public async Task<IActionResult> Logout([FromForm]LoginQuery query)
        {
            var result = await _security.Logout(query);
            return Ok(result);
        }
    }
}