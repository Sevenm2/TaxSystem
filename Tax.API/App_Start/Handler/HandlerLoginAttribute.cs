﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Tax.API.App_Start.Handler
{
    using Tax.Util;
    using Tax.Domain.Enum;

    public class HandlerLoginAttribute : TypeFilterAttribute
    {
        public HandlerLoginAttribute(LoginMode customMode) : base(typeof(HandlerLoginFilter))
        {
            Arguments = new object[] { customMode };
        }
    }

    public class HandlerLoginFilter : IAuthorizationFilter
    {
        readonly LoginMode _customMode;

        public HandlerLoginFilter(LoginMode customMode)
        {
            _customMode = customMode;
        }

        public void OnAuthorization(AuthorizationFilterContext filterContext)
        {
            if (_customMode == LoginMode.IGNORE)
            {
                return;
            }
            if (string.IsNullOrWhiteSpace(SessionInfo.LoginId))
            {
                filterContext.Result = new UnauthorizedResult();
            }
        }
    }
}
