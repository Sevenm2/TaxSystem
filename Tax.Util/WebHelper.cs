﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;

namespace Tax.Util
{
    public static class WebHelper
    {
        #region Session
        public static void WriteSession([CallerMemberName]string key = "", string value = "")
        {
            if (string.IsNullOrEmpty(key))
                return;
            HttpContext.Current.Session.SetString(key, value);
        }
        
        public static void WriteSession(string key, byte[] value = default(byte[]))
        {
            HttpContext.Current.Session.Set(key, value);
        }
       
        public static string GetSession([CallerMemberName]string key = "")
        {
            if (string.IsNullOrEmpty(key))
                return string.Empty;
            var value = HttpContext.Current.Session.GetString(key);
            if (value == null)
                value = string.Empty;
            return value;
        }
        public static void RemoveSession([CallerMemberName]string key = "")
        {
            if (string.IsNullOrEmpty(key))
                return;
            HttpContext.Current.Session.Remove(key);
        }

        #endregion

        #region Cookie
        public static void WriteCookie(string strName, string strValue)
        {
            HttpContext.Current.Response.Cookies.Append(strName, strValue);
        }

        public static void WriteCookie(string strName, string strValue, int expires)
        {
            HttpContext.Current.Response.Cookies.Append(strName, strValue, new CookieOptions
            {
                Expires = DateTime.Now.AddMinutes(expires)
            });
        }

        public static string GetCookie(string strName)
        {
            if (HttpContext.Current.Request.Cookies != null && HttpContext.Current.Request.Cookies[strName] != null)
            {
                return HttpContext.Current.Request.Cookies[strName];
            }
            return "";
        }

        public static void RemoveCookie(string CookiesName)
        {
            HttpContext.Current.Response.Cookies.Append(CookiesName.Trim(), "", new CookieOptions
            {
                Expires = DateTime.Now.AddYears(-5)
            });
        }
        #endregion
    }
}
