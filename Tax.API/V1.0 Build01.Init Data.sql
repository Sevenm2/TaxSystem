USE Taxation
GO

IF NOT EXISTS (SELECT 1 FROM City WHERE CityCode = 'GZ')
BEGIN
	INSERT INTO [dbo].[City]
	([CityCode], [CityName], [AnnuityRate], [MedicareRate], [UnemployRate], [FundRate], [Deleted])
	VALUES
	('BJ', 'Beijing', 8, 3, 0.5, 20, 0),
	('SH', 'Shanghai', 8, 2, 0.6, 15, 0),
	('GZ', 'Guangzhou', 10, 3, 0.5, 10, 0),
	('SZ', 'Shenzhen', 8, 2, 0.1, 12, 0)
END


IF NOT EXISTS (SELECT 1 FROM [dbo].[TaxRate] WHERE [MoneyLevel] = 5000)
BEGIN
	INSERT INTO [dbo].[TaxRate]
	([MoneyLevel], [Rate], [Offset], [Deleted])
	VALUES
	(3000, 3,0, 0),
	(12000, 10, 210, 0),
	(25000, 20, 1410, 0),
	(35000, 25, 2660, 0),
	(55000, 30, 4410, 0),
	(80000, 35, 7160, 0),
	(80001, 45, 15160, 0)
END

IF NOT EXISTS (SELECT 1 FROM [dbo].[User] WHERE [LoginID] = 'mark1')
BEGIN
	INSERT INTO [dbo].[User]
	([LoginID], [Password])
	VALUES
	('mark1', SUBSTRING(sys.fn_sqlvarbasetostr(HashBytes('MD5','123456')), 3, 32)),
	('mark2', SUBSTRING(sys.fn_sqlvarbasetostr(HashBytes('MD5','123456')), 3, 32))
END




