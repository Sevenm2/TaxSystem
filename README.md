# TaxSystem


The 2 services (API & Website) will be run in the docker as 2 containers.

The power shell command are as below.

## API
switch to the API project root path
```bash
cd D:\Mark\Git\TaxSystem
```
Build the API image
```bash
docker build -t markjiang/taxapi .
```
Run the API image
```bash
docker run -it -p 8090:8090 --name markjiang_taxapi markjiang/taxapi
```

## WebSite
switch to the Web project root path
```bash
cd D:\Mark\Git\TaxSystem\Tax.Web\ClientApp
```
Build the Web image
```bash
docker build -t -f Dockerfile.compile markjiang/taxweb .
```
Run the Web image
```bash
docker run -it -p 8090:8090 --name markjiang_taxweb markjiang/taxweb
```

The Redis will be used for the application session.

## Application Screenshot
![TaxSalary1](TaxContent/pic/TaxSalary1.png)
![TaxSalary2](TaxContent/pic/TaxSalary2.png)

## Deployment Screenshot
### Docker
![docker1](TaxContent/pic/docker1.png)
![docker2](TaxContent/pic/docker2.png)

### Redis
![Redis1](TaxContent/pic/Redis1.png)
![Redis2](TaxContent/pic/Redis2.png)