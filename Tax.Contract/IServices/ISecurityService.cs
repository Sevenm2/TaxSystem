﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Tax.Contract.IServices
{
    using Tax.Domain.Query;
    using Tax.Domain.Model;
    public interface ISecurityService
    {
        Task<LoginModel> CheckLogin(LoginQuery query);

        Task<LoginModel> Logout(LoginQuery query);
    }

}
