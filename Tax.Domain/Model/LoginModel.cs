﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tax.Domain.Model
{
    public class LoginModel
    {
        public string LoginID { get; set; }

        public int LoginResult { get; set; }
    }
}
