﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Cors;

namespace Tax.API.Controllers
{
    using Tax.API.App_Start.Handler;
    using Tax.Contract.IServices;
    using Tax.Domain.Enum;
    using Tax.Domain.Model;
    using Tax.Domain.Query;

    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("any")]
    [HandlerLogin(LoginMode.ENFORCE)]
    public class TaxController : ControllerBase
    {
        private IBusinessService _business;
        public TaxController(IBusinessService business)
        {
            _business = business;
        }

        //GET: api/tax/getCityInfo
        [HttpGet, Route("getCityInfo")]
        public async Task<IActionResult> GetCityInfo()
        {
            var result = await _business.GetCityInfo();
            if (result == null)
            {
                return NotFound();
            }
            return Ok(result);
        }

        //POST: api/tax/calculateTaxSalary
        [HttpPost, Route("calculateTaxSalary")]
        public async Task<IActionResult> CalculateTaxSalary(CalculateQuery query)
        {
            var result = await _business.CalculateTaxSalary(query);
            if (result == null)
            {
                return NotFound();
            }
            return Ok(result);
        }
    }
}